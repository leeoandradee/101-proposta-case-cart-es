package br.com.cartoes.services;

import br.com.cartoes.DTOs.CreditCardDTO;
import br.com.cartoes.DTOs.CreditCardUptadedDTO;
import br.com.cartoes.models.Client;
import br.com.cartoes.models.CreditCard;
import br.com.cartoes.repositories.ClientRepository;
import br.com.cartoes.repositories.CreditCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CreditCardService {

    @Autowired
    CreditCardRepository creditCardRepository;

    @Autowired
    ClientRepository clientRepository;

    public CreditCardDTO createCreditCard(CreditCardDTO creditCardDTO) {
        Optional<Client> clientOptional = clientRepository.findById(creditCardDTO.getClientId());

        if (clientOptional.isPresent()) {
            CreditCard creditCard = new CreditCard();
            creditCard.setNumber(creditCardDTO.getNumber());
            creditCard.setActive(true);
            creditCard.setClient(clientOptional.get());
            CreditCard creditCardCreated = creditCardRepository.save(creditCard);

            creditCardDTO.setActive(true);
            creditCardDTO.setId(creditCardCreated.getId());

            return creditCardDTO;
        } else {
            throw new RuntimeException("Client ID not found");
        }
    }

    public CreditCardDTO updateCreditCard(CreditCardUptadedDTO creditCardDTO, String number) {
        Optional<CreditCard> creditCardOptional = creditCardRepository.findByNumber(number);
        if (creditCardOptional.isPresent()) {
            CreditCard creditCard = creditCardOptional.get();
            creditCard.setActive(creditCardDTO.isActive());
            creditCardRepository.save(creditCardOptional.get());

            CreditCardDTO creditCardDTO1 = new CreditCardDTO();
            creditCardDTO1.setId(creditCard.getId());
            creditCardDTO1.setNumber(creditCard.getNumber());
            creditCardDTO1.setClientId(creditCard.getClient().getId());

            return creditCardDTO1;
        } else {
            throw new RuntimeException("Credit Card number not found");
        }
    }

    public CreditCardDTO findByNumber(String number) {
        Optional<CreditCard> creditCardOptional = creditCardRepository.findByNumber(number);

        if (creditCardOptional.isPresent()) {
            CreditCard creditCard = creditCardOptional.get();

            CreditCardDTO creditCardDTO = new CreditCardDTO();
            creditCardDTO.setId(creditCard.getId());
            creditCardDTO.setNumber(creditCard.getNumber());
            creditCardDTO.setActive(creditCard.isActive());
            creditCardDTO.setClientId(creditCard.getClient().getId());

            return creditCardDTO;
        } else {
            throw new RuntimeException("Credit Card number not found");
        }
    }



}
