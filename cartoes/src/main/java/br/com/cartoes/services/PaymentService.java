package br.com.cartoes.services;

import br.com.cartoes.DTOs.PaymentDTO;
import br.com.cartoes.models.CreditCard;
import br.com.cartoes.models.Payment;
import br.com.cartoes.repositories.CreditCardRepository;
import br.com.cartoes.repositories.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PaymentService {

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    CreditCardRepository creditCardRepository;

    public PaymentDTO createPaymente(PaymentDTO paymentDTO) {
        Optional<CreditCard> creditCardOptional = creditCardRepository.findById(paymentDTO.getCreditCardId());
        if (creditCardOptional.isPresent()) {

            Payment payment = new Payment();
            payment.setDescription(paymentDTO.getDescription());
            payment.setValue(paymentDTO.getValue());
            payment.setCreditCard(creditCardOptional.get());

            Payment paymentCreated = paymentRepository.save(payment);
            paymentDTO.setId(paymentCreated.getId());

            return paymentDTO;
        } else {
            throw new RuntimeException("Credit Card ID not found");
        }
    }

    public List<PaymentDTO> listPaymentsByCreditCardId(int id) {
        //List<Payment> payments = (List<Payment>) paymentRepository.findAll();
        List<Payment> payments = (List<Payment>) paymentRepository.findByCreditCardId(id);

        List<PaymentDTO> paymentDTOS = new ArrayList<>();

        for (Payment payment: payments) {
            PaymentDTO paymentDTO = new PaymentDTO();
            paymentDTO.setId(payment.getId());
            paymentDTO.setCreditCardId(payment.getCreditCard().getId());
            paymentDTO.setDescription(payment.getDescription());
            paymentDTO.setValue(payment.getValue());

            paymentDTOS.add(paymentDTO);
        }

        return paymentDTOS;
    }

}
