package br.com.cartoes.repositories;

import br.com.cartoes.models.CreditCard;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CreditCardRepository extends CrudRepository<CreditCard, Integer> {

    Optional<CreditCard> findByNumber(String number);

}
