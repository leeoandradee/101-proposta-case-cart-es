package br.com.cartoes.DTOs;

import javax.validation.constraints.NotNull;

public class CreditCardUptadedDTO {

    @NotNull(message = "Active can't be null")
    private boolean active;

    public CreditCardUptadedDTO() {
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
