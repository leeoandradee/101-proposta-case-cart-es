package br.com.cartoes.DTOs;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class PaymentDTO {

    private int id;

    @NotNull(message = "Credit Card ID can't be null")
    private int creditCardId;

    @NotNull(message = "Description can't be null")
    @NotEmpty(message = "Description can't be empty")
    private String description;

    @NotNull(message = "Value can't be null")
    private double value;

    public PaymentDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCreditCardId() {
        return creditCardId;
    }

    public void setCreditCardId(int creditCardId) {
        this.creditCardId = creditCardId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
