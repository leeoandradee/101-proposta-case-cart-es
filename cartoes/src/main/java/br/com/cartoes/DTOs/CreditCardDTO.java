package br.com.cartoes.DTOs;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class CreditCardDTO {

    private int id;

    @NotNull(message = "Credit Card number can't be null")
    @NotEmpty(message = "Credit Card number can't be empty")
    private String number;

    @NotNull(message = "Client ID can't be null")
    private int clientId;

    private boolean active;

    public CreditCardDTO() {
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
